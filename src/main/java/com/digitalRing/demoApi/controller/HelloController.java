package com.digitalRing.demoApi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class HelloController {

    @GetMapping("/hello")
    public String welcome() {
        return "Hello from Api Stream";
    }

}
